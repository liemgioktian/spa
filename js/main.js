var root = null
var useHash = true // Defaults to: false
var hash = '#!' // Defaults to: '#'
var router = new Navigo(root, useHash, hash)

$.getJSON(`${config.app_url}server/router.json`, function (routes) {
	const addresses = Object.keys(routes)
	var routerObj = {}
	function assignRouteObj (index)
	{
		var addr = addresses[index]
		var tpl = routes[addr].template
		var cbFn= ``
		var source = `${config.app_url}${routes[addr].source}`
		switch (tpl)
		{
			case 'dashboard': cbFn = buildDashboard;break
			case 'table': cbFn = buildTable;break
			case 'form': cbFn = buildForm;break
		}
		if ('table' === tpl) cbFn = buildTable
		routerObj[addr] = function ()
		{
			$.get(`template/${tpl}.html`, function (html) {
			  const compiled = Handlebars.compile(html)
			  const result = compiled({})
			  $(`.content`).html(result)
			  cbFn(source)
		  })
		}
		index++
		if (addresses[index]) assignRouteObj(index)
	}
	assignRouteObj(0)
	router.on(routerObj).resolve()
})

function buildDashboard(source)
{
	$.getJSON(source, function () {

	})
}

function buildTable(source)
{
	$('.table-model').DataTable({
	  columns: [{"mData":"orders","sTitle":"No","visible":false},{"mData":"nama_jamaah","sTitle":"Nama"},{"mData":"posisi","sTitle":"Posisi"},{"mData":"nama","sTitle":"Level","name":"level.nama"}]
	})
}

function buildForm(source)
{

}